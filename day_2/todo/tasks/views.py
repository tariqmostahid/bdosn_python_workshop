from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.views.generic.base import TemplateView

from .models import Task
from .forms import TaskForm

# Create your views here.

# class IndexView(TemplateView):
#     template_name = 'tasks/list.html'

def index(request):
    template_name = 'tasks/list.html'
    form = TaskForm()
    tasks = Task.objects.all()

    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('/')

    context = {
        'tasks': tasks,
        'form': form
    }

    return render(request, template_name , context)

def update_task(request, pk):
    template_name = 'tasks/update_task.html'
    task = Task.objects.get(id=pk)
    form =  TaskForm(instance=task)

    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
        return redirect('/')

    context = {
        'form': form
    }

    return render(request, template_name , context)


def delete_task(request, pk):
    template_name = "tasks/delete.html"
    task = Task.objects.get(id=pk)

    if request.method == "POST":
        task.delete()
        return redirect('/')

    context = { 'task': task}

    return render(request, template_name , context)
