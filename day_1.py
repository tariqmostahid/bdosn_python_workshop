# x = 1 # int
# y = 2.5 # float
# name = "Alom"  # str
# is_summer = False # boolean

# # multiple assignment

# x, y, name, is_summer = (12, 12.6, "Alom", False)

# xx = "1"
# # Type casting 

# x = int(xx)
# y = float(x)
# s = str(y)


# print(x)
# print(type(x))
# print(type(xx))

# print(y)
# print(s)
# print("type of s: " + str(type(s)))
# print(name)
# print(is_summer)

# this is a comment

'''
    this 
    is 
    a 
    multiline 
    comment
'''

"""
    this 
    is 
    a 
    multiline 
    comment
"""


# String

# name = "Alim"
# age = 28

# # concatenate

# # print("hello!!, my name is " + name + "and my age is " + str(age))


# # formating

# # print("hello!!, my name {n} is and my age is {a}".format(n=name, a=age) )

# s = "hello world"

# print(s.capitalize())
# print(s.upper())
# print(len(s))
# print(s.replace('hello', 'Hi!!'))

# sub = "l"
# print(s.count(sub))

# print(s.split())

# print(s.find('w'))
# print(s.isalpha())
# print(s.isnumeric())
# print(s.isalnum())



# List

# number = [1 , 2, 3, 4]

# fruits = ['apple', 'orange', 'banana']

# print(len(fruits))
# print(fruits[1])

# fruits.append('grapes')
# fruits.remove('apple')
# print(fruits)
# fruits.insert(2, 'cherry')
# fruits[1] = 'strawberry'

# fruits.sort()
# fruits.sort(reverse=True)
# print(fruits)



# tuples

# fruits = ('apple', 'orange', 'banana')

# fruits2 = ('orange', 'banana')

# print(fruits[1])
# # fruits[0] = 'cherry'
# print(fruits2)
# del fruits2
# # print(fruits2)


# set

# fruits_set = {'apple', 'orange', 'banana'}
# fruits_set2 = {'apple', 'orange', 'banana'}

# # print(fruits_set)

# fruits_set.add('graps')

# fruits_set.remove('apple')

# fruits_set.add('graps')
# # fruits_set2.clear()
# # del fruits_set2
# print(fruits_set2)


# ditionary 

# p = {
#     'first_name': 'Alim',
#     'last_name': 'islam',
#     'age': 34,
#     1: 1
# }

# print(p['first_name'])
# print(p.get('age'))

# p['phone'] = "0193747493"

# print(p)

# print(p.keys())

# p2 = p.copy()
# print(p2)

# print(len(p2))

# list_of_dictionaty = [
#     {'first_name': 'Alim', 'last_name': 'islam', 'age': 34},
#     {'first_name': 'Alom', 'last_name': 'islam', 'age': 30},
# ]

# print(list_of_dictionaty[1])

# phn =p.pop('phone')

# print(phn)
# print(p)


# conditionals

# val = 100

# if val:
#     print("val got true")
#     print(val)

# # if / else

# a = 101

# if a < 100:
#     print("value is less than 100.")
# elif a == 100:
#     print("value is equal 100. ")
# else:
#     print("value is greater than 100.")

# loop

# cnt = 0

# while cnt < 9:
#     print("count: ", cnt)
#     cnt += 1
# else:
#     print(cnt)


# for ltr in 'python':
#     print('current letter: ', ltr)

fruits = ['apple', 'orange', 'banana']

# for index in range(len(fruits)):
#     print(index)
#     print(fruits[index]) # fruits[1]

# for fruit in fruits:
#     print(fruit)

# lngth = len(fruits)
# print(lngth)

# for i in range(0, lngth+3, 2):
#     print(i)

# function

# def sayHello(name):
#     print("hello to!!! {name}".format(name=name))


# # sayHello("Alim")


# def getSum(val1, val2):
#     # total = val1 + val2
#     # return total
#     return val1 + val2

# s = getSum(2, 4)

# print(s)


# class 

class User:
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age

    def greetings(self):
        return print("hello!! " + self.name)

alim = User('Alim', 'alim@domain.com', 30)
alim.greetings()


class Customer(User):
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age
        self.balance = 0

    def set_balance(self, balance):
        self.balance = balance

    def greetings(self):
        return print("Hello {name} your balance is {balance}".format(name=self.name, balance=self.balance))

    

c1 = Customer("alom", "alom@domain.com", 20)
c1.set_balance(1000)
c1.greetings()


